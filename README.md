# Requirements

* PHP 8.0 or greater with cURL and openssl support

# Setup

* In `back`, create a `data` directory and create two JSON files
    
    ---
    ```json
    {
        "users": {
            "meta": {
                "id_field": "user_id",
                "auto_increment": true
            },
            "data": []
        },
        "preferences": {
            "meta": {
                "id_field": "preferences_id",
                "auto_increment": true
            },
            "data": []
        }
    }
    ```
    * `users.json` with the above structure
    
    ---
    ```json
    {
        "session": {
            "meta": {
                "id_field": "session_id",
                "auto_increment": false
            },
            "data": []
        }
    }
    ```
    * `session.json` with the above structure
    ---
* In `front`, run `npm install` to get all the required dependencies.

# Usage

* In one terminal, navigate to the `back` folder and run `php -S localhost:8080`
    * This will spin up the default PHP builtin server on localhost, port 8080

* In another terminal, navigate to the `front` directory
    * Run `npm install`
    * Run `ng serve`
        * This will spin up the front end on `http://localhost:4200`