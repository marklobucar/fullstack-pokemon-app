<?php

define('BASE_APP_URL', 'http://localhost:4200');

define('DATA_STORAGE_PATH', __DIR__.'/data');

define('DUMP_ERROR_DATA', true);

define('LOG_DISABLED', false);