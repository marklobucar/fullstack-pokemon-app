<?php declare(strict_types=1);

use App\DI\Container;


Container::set('dbAdapter', new \App\Db\FilesystemDbAdapter());

$marshallingService = new \App\Marshalling\MarshallingService();
$marshallingService->registerMarshaller(new \App\Pokemon\PokemonShortMarshaller(), 'pokemon-short');

Container::set('marshallingService', $marshallingService);

Container::set('sessionService', new \App\Session\SessionService());
Container::set('usersService', new \App\Users\UsersService());
Container::set('usersRestHandler', new \App\Users\UsersRestHandler());
Container::set('authRestHandler', new \App\Auth\AuthRestHandler());

Container::set('pokemonApi', new \App\Pokemon\PokemonApi());
Container::set('pokemonService', new \App\Pokemon\PokemonService());
Container::set('pokemonRestHandler', new \App\Pokemon\PokemonRestHandler());