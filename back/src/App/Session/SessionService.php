<?php declare(strict_types=1);

namespace App\Session;

use App\Db\ComparisonOperator;
use App\Db\LogicalOperator;
use App\DI\Container;

class SessionService
{
    private const DB_SESSION = 'session';
    private const TABLE_SESSION = 'session';

    private const MONTH = 60 * 60 * 24 * 30; 

    /**
     * @var \App\Db\IDbAdapter
     */
    private $_dbAdapter;

    public function __construct()
    {
        $this->_dbAdapter = Container::get('dbAdapter');
    }

    public function getSessionById($sessionId)
    {
        $session = $this->_dbAdapter->select(
            self::DB_SESSION,
            self::TABLE_SESSION,
            [],
            new ComparisonOperator('session_id', $sessionId, ComparisonOperator::OPERATOR_EQ)
        );

        return $session;
    }

    public function createNewSession($userId)
    {
        $session_id = $this->_uuidV4();

        $new_session = [
            'session_id' => $session_id,
            'active' => 1,
            'user_id' => $userId,
            'expires' => time() + self::MONTH
        ];

        $this->_dbAdapter->insert(
            self::DB_SESSION,
            self::TABLE_SESSION,
            $new_session
        );

        return $session_id;
    }

    public function closeSession($sessionId)
    {
        $this->_dbAdapter->update(
            self::DB_SESSION,
            self::TABLE_SESSION,
            ['active' => 0],
            new ComparisonOperator('session_id', $sessionId, ComparisonOperator::OPERATOR_EQ)
        );
    }

    public function isSessionExpired($sessionId)
    {
        $session = $this->_dbAdapter->select(
            self::DB_SESSION,
            self::TABLE_SESSION,
            [],
            new LogicalOperator(LogicalOperator::OPERATOR_AND,
                [
                    new ComparisonOperator('session_id', $sessionId, ComparisonOperator::OPERATOR_EQ),
                    new ComparisonOperator('expires', time(), ComparisonOperator::OPERATOR_GT)
                ]
            )
        );

        return !empty($session);
    }

    public function findExistingActiveSession($userId)
    {
        $session = $this->_dbAdapter->select(
            self::DB_SESSION,
            self::TABLE_SESSION,
            [],
            new LogicalOperator(LogicalOperator::OPERATOR_AND,
                [
                    new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ), // for given user
                    new ComparisonOperator('active', 1, ComparisonOperator::OPERATOR_EQ), // not logged out
                    new ComparisonOperator('expires', time(), ComparisonOperator::OPERATOR_GT) // not expired
                ]
            )
        );

        if (count($session) > 1) {
            throw new \Exception('Too many active sessions for user ['.$userId.']');
        }

        if (!empty($session)) {
            return $session[0];
        }

        return null;
    }

    private function _uuidV4()
    {
        $data = openssl_random_pseudo_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}