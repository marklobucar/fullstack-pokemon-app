<?php declare(strict_types=1);

namespace App\Http;

use App\Log\EchoLogger;

class SimpleHTTPClient
{
    public function __construct()
    {
    }

    public function sendRequest($method, $url, $body = [], $headers = [])
    {
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);

            if (!empty($headers)) {
                $formatted_headers = [];

                foreach ($headers as $header => $value) {
                    $formatted_headers[] = "$header: $value";
                }

                curl_setopt($ch, CURLOPT_HTTPHEADER, $formatted_headers);
            }

            if ($method === 'POST') {
                curl_setopt($ch, CURLOPT_POST,  true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            }

            if ($method === 'PUT') {
                curl_setopt($ch, CURLOPT_PUT, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            }

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            curl_close($ch);

            return json_decode($response, true);
        } catch (\Exception $e) {
            EchoLogger::log($e);
            return [];
        }
    }
}
