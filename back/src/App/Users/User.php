<?php declare(strict_types=1);

namespace App\Users;

class User
{
    private $_userId;
    private $_email;
    private $_password;
    private $_favoritePokemon;

    public function __construct($userId, $email, $password, $favoritePokemon)
    {
        $this->_userId = $userId;
        $this->_email = $email;
        $this->_password = $password;
        $this->_favoritePokemon = $favoritePokemon;
    }

    public function getData($includePassword = false)
    {
        $data = [
            'user_id' => $this->_userId,
            'email' => $this->_email,
            'favorite_pokemon' => $this->_favoritePokemon
        ];

        if ($includePassword) {
            $data['password'] = $this->_password;
        }

        return $data;
    }

    public function getUserId()
    {
        return intval($this->_userId);
    }

    public function getEmail() 
    {
        return $this->_email;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function getFavoritePokemon()
    {
        return $this->_favoritePokemon;
    }

    public function setFavoritePokemon($data)
    {
        $this->_favoritePokemon = $data;
    }
}