<?php declare(strict_types=1);

namespace App\Users;

use App\DI\Container;
use App\Log\EchoLogger;
use App\Rest\AbstractRestHandler;
use App\Rest\NotFoundException;
use App\Rest\RequestInfo;
use App\Rest\SimpleRequest;
use App\Rest\SimpleResponse;

class UsersRestHandler extends AbstractRestHandler
{
    /**
     * @var \App\Users\UsersService
     */
    private $_usersService;

    /**
     * @var \App\Pokemon\PokemonService
     */
    private $_pokemonService;

    /**
     * @var \App\Session\SessionService
     */
    private $_sessionService;

    /**
     * @var \App\Marshalling\MarshallingService
     */
    private $_marshallingService;

    public function __construct()
    {
        $this->_usersService = Container::get('usersService');
        $this->_pokemonService = Container::get('pokemonService');
        $this->_sessionService = Container::get('sessionService');
        $this->_marshallingService = Container::get('marshallingService');
    }

    protected function _handle(SimpleRequest $request): SimpleResponse
    {
        $info = new RequestInfo($request);

        if ($request->getMethod() === 'GET' && $info->route('users/{userId}')) {
            return $this->_handleUsersPathUserIdGet($request, intval($info->get('userId')));
        }
        
        if ($request->getMethod() === 'DELETE' && $info->route('users/{userId}')) {
            return $this->_handleUsersPathUserIdDelete($request, intval($info->get('userId')));
        }

        if ($request->getMethod() === 'PUT' && $info->route('users-preferences/{userId}')) {
            return $this->_handleUsersPreferencesPathUserIdPut($request, intval($info->get('userId')));
        }

        throw new NotFoundException('Could not map ['.$info.']');
    }

    private function _handleUsersPathUserIdGet(SimpleRequest $request, $userId)
    {
        $user = $this->_usersService->getUserById($userId);
        unset($user['password']);

        return new SimpleResponse($user);
    }

    private function _handleUsersPathUserIdDelete(SimpleRequest $request, $userId)
    {
        EchoLogger::log('Deleting user ['.$userId.']');

        $this->_usersService->deleteUser($userId);

        $this->_usersService->deleteUserPreferences($userId);

        $session_id = $request->getHeader('App-Session-Id');
        $this->_sessionService->closeSession($session_id);

        return new SimpleResponse([]);
    }

    private function _handleUsersPreferencesPathUserIdPut(SimpleRequest $request, $userId)
    {
        $json = $request->getBody();

        EchoLogger::log('Updating user preferences ['.$userId.']['.print_r($json, true).']');

        $this->_usersService->updateUserPreferences($userId, $json);
        $user = $this->_usersService->getUserById($userId);

        $pokemon_details = $this->_pokemonService->getPokemonById($json['favorite_pokemon_id']);
        $user->setFavoritePokemon($this->_marshallingService->marshalItem($pokemon_details, 'pokemon-short'));

        return new SimpleResponse($user->getData());
    }
}