<?php declare(strict_types=1);

namespace App\Users;

use App\Db\ComparisonOperator;
use App\DI\Container;
use App\Log\EchoLogger;
use App\Rest\BadRequestException;
use App\Rest\NotFoundException;

class UsersService
{
    private const DB_USERS = 'users';
    private const TABLE_USERS = 'users';
    private const TABLE_PREFERENCES = 'preferences';

    /**
     * DB Adapter
     * @var \App\Db\IDbAdapter
     */
    private $_dbAdapter;

    public function __construct()
    {
        $this->_dbAdapter = Container::get('dbAdapter');    
    }

    #region USER User related calls

    public function getUserByEmail($email): User
    {
        $user = $this->_dbAdapter->select(
            self::DB_USERS,
            self::TABLE_USERS,
            [],
            new ComparisonOperator('email', $email, ComparisonOperator::OPERATOR_EQ)
        );

        if (empty($user)) {
            throw new NotFoundException('No user with email ['.$email.']');
        }

        if (count($user) > 1) {
            throw new BadRequestException('Too many users with email ['.$email.']');
        }

        return $this->_deserializeUser($user[0]);
    }

    public function getUserById($userId): User
    {
        $user = $this->_dbAdapter->select(
            self::DB_USERS,
            self::TABLE_USERS,
            [],
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );

        if (empty($user)) {
            throw new NotFoundException('No user with ID ['.$userId.']');
        }

        if (count($user) > 1) {
            throw new BadRequestException('Too many users with ID ['.$userId.']');
        }

        return $this->_deserializeUser($user[0]);
    }

    public function createNewUser($email, $password)
    {
        $id = $this->_dbAdapter->insert(self::DB_USERS, self::TABLE_USERS, [
            'email' => $email,
            'password' => md5($password) // extremely complicated hashing algorithm
        ]);

        $this->_dbAdapter->insert(self::DB_USERS, self::TABLE_PREFERENCES, [
            'user_id' => $id,
            'favorite_pokemon_id' => null
        ]);

        return $id;
    }

    public function deleteUser($userId)
    {
        $this->_dbAdapter->delete(
            self::DB_USERS,
            self::TABLE_USERS,
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );

        $this->_dbAdapter->delete(
            self::DB_USERS,
            self::TABLE_PREFERENCES,
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );
    }

    #endregion USER

    #region USER-PREFERENCES User preferences related calls

    public function getUserPreferences($userId)
    {
        $user_preferences = $this->_dbAdapter->select(
            self::DB_USERS,
            self::TABLE_PREFERENCES,
            [],
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );

        if (empty($user_preferences)) {
            throw new NotFoundException('User ['.$userId.'] has no preferences.');
        }

        if (count($user_preferences) > 1) {
            throw new BadRequestException('Too many user preferences objects for user ['.$userId.']');
        }

        return $user_preferences[0];
    }

    public function updateUserPreferences($userId, $data)
    {
        $existing = $this->_dbAdapter->select(
            self::DB_USERS,
            self::TABLE_PREFERENCES,
            [],
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );

        if (count($existing) > 1) {
            throw new BadRequestException('Too many user preferences objects for user ['.$userId.']');
        }

        if (empty($existing)) {
            $new = array_merge(['user_id' => $userId], $data);
            return $this->_dbAdapter->insert(
                self::DB_USERS,
                self::TABLE_PREFERENCES,
                $new
            );
        }

        $this->_dbAdapter->update(
            self::DB_USERS,
            self::TABLE_PREFERENCES,
            array_merge($existing[0], $data),
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );
    }

    public function deleteUserPreferences($userId)
    {
        $count = $this->_dbAdapter->delete(
            self::DB_USERS,
            self::TABLE_PREFERENCES,
            new ComparisonOperator('user_id', $userId, ComparisonOperator::OPERATOR_EQ)
        );

        return $count;
    }

    #endregion USER-PREFERENCES

    private function _deserializeUser($userData)
    {
        $user_preferences = $this->_dbAdapter->select(
            self::DB_USERS,
            self::TABLE_PREFERENCES,
            [],
            new ComparisonOperator('user_id', $userData['user_id'], ComparisonOperator::OPERATOR_EQ)
        );

        $actual = new User(
            $userData['user_id'],
            $userData['email'],
            $userData['password'],
            $user_preferences[0]['favorite_pokemon_id'] ?? null
        );

        return $actual;
    }
}