<?php declare(strict_types=1);

namespace App\Db;

use App\Log\EchoLogger;

class ComparisonOperator implements IOperator
{
    public const OPERATOR_EQ = '=';
    public const OPERATOR_LT = '<';
    public const OPERATOR_LTE = '<=';
    public const OPERATOR_GT = '>';
    public const OPERATOR_GTE = '>=';
    public const OPERATOR_NEQ = '!=';

    private $_field;
    private $_operator;
    private $_value;

    public function __construct($field, $value, $operator)
    {
        $this->_field = $field;
        $this->_value = $value;
        $this->_operator = $operator;
    }

    public function compare($data)
    {
        if (empty($data)) {
            return false;
        }

        EchoLogger::log('Comparing ['.$this->_field.']['.$this->_operator.'][('.gettype($this->_value).') '.$this->_value.'] against actual [('.gettype($data[$this->_field]).') '.$data[$this->_field].']');
        
        switch ($this->_operator)
        {
            case self::OPERATOR_EQ: 
                return $data[$this->_field] === $this->_value;
            case self::OPERATOR_NEQ:
                return $data[$this->_field] !== $this->_value;
            case self::OPERATOR_LT:
                return $data[$this->_field] < $this->_value;
            case self::OPERATOR_LTE:
                return $data[$this->_field] <= $this->_value;
            case self::OPERATOR_GT:
                return $data[$this->_field] > $this->_value;
            case self::OPERATOR_GTE:
                return $data[$this->_field] >= $this->_value;
            default:
                throw new \Exception('Unexpected operator ['.$this->_operator.']');
        }
    }

    public function __toString()
    {
        return get_class($this).'[['.$this->_field.']['.$this->_operator.']['.$this->_value.']]';
    }
}