<?php declare(strict_types=1);

namespace App\Db;

interface IOperator
{
    /**
     * Execute comparison on a set of data
     * @param array $data Data to compare on
     * @return bool Comparison result
     */
    public function compare(array $data);
}