<?php declare(strict_types=1);

namespace App\Db;

use App\Log\EchoLogger;

class LogicalOperator implements IOperator
{
    public const OPERATOR_AND = 'AND';
    public const OPERATOR_OR = 'OR';

    private $_operator;

    /**
     * @var IOperator[]
     */
    private $_comparisons;

    public function __construct($operator, $comparisons)
    {
        $this->_operator = $operator;
        $this->_comparisons = $comparisons;
    }

    public function compare($data)
    {
        $result = null;

        foreach ($this->_comparisons as $comparison)
        {
            if ($result === null) {
                $result = $comparison->compare($data);
                EchoLogger::log('Set first ['.$this->_operator.'] comparison to ['.($result ? 'true':'false').']');
            } else {
                switch ($this->_operator)
                {
                    case self::OPERATOR_AND:
                        $result = $result && $comparison->compare($data);
                        break;
                    case self::OPERATOR_OR:
                        $result = $result || $comparison->compare($data);
                        break;
                    default:
                        throw new \Exception('Unexpected logical operator ['.$this->_operator.']');
                }

                EchoLogger::log('Current ['.$this->_operator.'] comparison is ['.($result ? 'true':'false').']');
            }
        }

        return $result;
    }

    public function __toString()
    {
        return get_class($this).implode($this->_operator, $this->_comparisons);
    }
}