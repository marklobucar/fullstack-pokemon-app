<?php declare(strict_types=1);

namespace App\Db;

interface IDbAdapter
{
    /**
     * Query the specified DB and table for some data
     * @param string $dbName Name of the database to query
     * @param string $tableName Name of the table to query
     * @param array $fields Names of fields to select. If empty, return everything.
     * @param IOperator $operator Array of operators to write
     * @return array Data that was selected 
     */
    public function select($dbName, $tableName, $fields = [], IOperator $operator = null);

    /**
     * Insert data into the specified table on the given DB
     * @param string $dbName Name of the database to query
     * @param string $tableName Table into which to insert data
     * @param array $data Data to insert
     * @return int|string|bool Returns the newly created ID on successful creation, false otherwise
     */
    public function insert($dbName, $tableName, $data);

    /**
     * Update a specific part of the table based on some conditions
     * @param string $dbName Name of the database to query
     * @param string $tableName Table on which to perform the update
     * @param array $data Data to update
     * @param IOperator $operator A condition to apply to decide what to update
     * @return int|bool Returns the count of updated items on success, false otherwise
     */
    public function update($dbName, $tableName, $data, IOperator $operator = null);

    /**
     * Delete a specified part in the given table on the specified DB
     * @param string $dbName Name of the database to query
     * @param string $tableName Table name in which to delete data
     * @param IOperator $operator Condition to apply to decide what to delete
     * @return int|bool Return the amount of deleted items on success, false otherwise
     */
    public function delete($dbName, $tableName, IOperator $operator = null);
}