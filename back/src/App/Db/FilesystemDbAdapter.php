<?php declare(strict_types=1);

namespace App\Db;

class FilesystemDbAdapter implements IDbAdapter
{
    public function __construct()
    {
    }

    public function select($dbName, $tableName, $fields = [], IOperator $operator = null)
    {
        $db = $this->_openDb($dbName);
        $table = $db[$tableName]["data"];

        $return = [];

        foreach ($table as $item) {
            $should_return = $operator !== null ? $operator->compare($item) : true;

            if ($should_return) {
                if (!empty($fields)) {
                    $formatted = [];
                    
                    foreach ($fields as $field) {
                        $formatted[$field] = $item[$field];
                    }
                } else {
                    $formatted = $item;
                }

                $return[] = $formatted;
            }
        }

        return $return;
    }

    public function insert($dbName, $tableName, $data)
    {
        $db = $this->_openDb($dbName);
        
        $table = $db[$tableName]["data"];
        $meta = $db[$tableName]["meta"];
        
        $new_id = null;

        if ($meta['auto_increment']) {
            $new_id = $this->_findLastId($table, $meta['id_field']) + 1;

            if (intval($new_id) <= 0) {
                $new_id = "1";
            }

            $data = array_merge($data, [$meta['id_field'] => $new_id]);
        } else if ($data[$meta['id_field']]) {
            $new_id = $data[$meta['id_field']];
        } else {
            throw new \Exception('No ID field present.');
        }

        array_push($table, $data);
        $db[$tableName]["data"] = $table;
        $this->_saveDb($dbName, $db);

        return $new_id;
    }

    public function update($dbName, $tableName, $data, IOperator $operator = null)
    {
        $db = $this->_openDb($dbName);
        
        $table = $db[$tableName]["data"];

        $update_indices = [];

        foreach ($table as $index => $item) {
            if ($operator) {
                if ($operator->compare($item)) {
                    $update_indices[] = $index;
                }
            } else {
                $update_indices[] = $index;
            }
        }

        foreach ($update_indices as $index) {
            $table[$index] = array_merge($table[$index], $data);
        }

        $db[$tableName]["data"] = $table;
        $this->_saveDb($dbName, $db);

        return count($update_indices);
    }

    public function delete($dbName, $tableName, IOperator $operator = null)
    {
        $db = $this->_openDb($dbName);
        
        $table = $db[$tableName]["data"];

        $delete_indices = [];

        foreach ($table as $index => $item) {
            if ($operator) {
                if ($operator->compare($item)) {
                    $delete_indices[] = $index;
                }
            } else {
                $delete_indices[] = $index;
            }
        }

        foreach ($delete_indices as $index) {
            $table[$index] = null;
        }

        $table = array_filter($table, function($item) {
            return $item !== null;
        });

        $db[$tableName]["data"] = $table;
        $this->_saveDb($dbName, $db);

        return count($delete_indices);
    }

    private function _openDb($dbName)
    {
        $db_path = DATA_STORAGE_PATH.'/'.$dbName.'.json';

        if (!file_exists($db_path)) {
            touch($db_path);
            file_put_contents($db_path, '{}');
        }

        return json_decode(file_get_contents($db_path), true);
    }

    private function _saveDb($dbName, $data)
    {
        $db_path = DATA_STORAGE_PATH.'/'.$dbName.'.json';

        if (!file_exists($db_path)) {
            touch($db_path);
        }

        file_put_contents($db_path, json_encode($data));
    }

    private function _findLastId($table, $idName)
    {
        if (empty($table)) {
            return 0;
        }

        $all = $table;
        usort($all, function($a, $b) use ($idName) {
            return intval($a[$idName]) - intval($b[$idName]);
        });

        return array_pop($all)[$idName];
    }
}