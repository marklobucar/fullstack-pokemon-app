<?php declare(strict_types=1);

namespace App\Pokemon;

use App\Auth\NotAuthorizedException;
use App\DI\Container;
use App\Log\EchoLogger;
use App\Rest\AbstractRestHandler;
use App\Rest\RequestInfo;
use App\Rest\SimpleRequest;
use App\Rest\SimpleResponse;

class PokemonRestHandler extends AbstractRestHandler
{
    /**
     * @var \App\Pokemon\PokemonService
     */
    private $_pokemonService;

    /**
     * @var \App\Session\SessionService
     */
    private $_sessionService;

    /**
     * @var \App\Marshalling\MarshallingService
     */
    private $_marshallingService;

    public function __construct()
    {
        $this->_pokemonService = Container::get('pokemonService');
        $this->_sessionService = Container::get('sessionService');
        $this->_marshallingService = Container::get('marshallingService');
    }

    protected function _handle(SimpleRequest $request): SimpleResponse
    {
        $this->_checkSessionExpired($request);

        $info = new RequestInfo($request);

        if ($request->getMethod() === 'GET' && $info->route('pokemon')) {
            return $this->_handlePokemonGet($request);
        }

        if ($request->getMethod() === 'GET' && $info->route('pokemon/{pokemonId}')) {
            return $this->_handlePokemonPathPokemonIdGet($request, $info->get('pokemonId'));
        }
    }

    private function _handlePokemonGet(SimpleRequest $request): SimpleResponse
    {
        $query = $request->getQueryParams();

        EchoLogger::log('Got query params ['.print_r($query, true).']');

        $return = [];

        $list = $this->_pokemonService->getPokemon($query['offset'] ?? 0, $query['limit'] ?? 0);

        foreach ($list['results'] as $pokemon) {
            $details = $this->_pokemonService->getPokemonById($this->_getPokemonIdFromURL($pokemon['url']));
            $deserialized = $this->_marshallingService->marshalItem($details, 'pokemon-short');

            // EchoLogger::log('Deserialized pokemon ['.print_r($deserialized, true).']');
            $return[] = $deserialized;
        }

        $response = new SimpleResponse($return);
        $response->setHeader('X-Pokemon-Count', $list['count']);

        return $response;
    }

    private function _handlePokemonPathPokemonIdGet(SimpleRequest $request, $pokemonId): SimpleResponse
    {
        return new SimpleResponse([]);
    }

    // UTIL
    private function _getPokemonIdFromURL($url)
    {
        $parts = explode('/', $url);
        $parts = array_reverse($parts);

        return $parts[1];
    }

    private function _checkSessionExpired(SimpleRequest $request)
    {
        $session_id = $request->getHeader('App-Session-ID');

        if (!$session_id) {
            throw new NotAuthorizedException('Missing session ID header.');
        }

        if ($this->_sessionService->isSessionExpired($session_id)) {
            $this->_sessionService->closeSession($session_id);
            throw new NotAuthorizedException('Session has expired.');
        }
    }
}
