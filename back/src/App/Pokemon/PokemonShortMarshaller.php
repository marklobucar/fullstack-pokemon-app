<?php declare(strict_types=1);

namespace App\Pokemon;

use App\Marshalling\IMarshaller;

class PokemonShortMarshaller implements IMarshaller
{
    public function __construct()
    {
    }

    public function marshal($data)
    {
        $marshalled = [
            'id' => $data['id'],
            'name' => $data['name'],
            'types' => array_map(function ($type) { return $type['type']['name']; }, $data['types']),
            'image' => $data['sprites']['front_default']
        ];

        return $marshalled;
    }
}