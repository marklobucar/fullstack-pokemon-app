<?php declare(strict_types=1);

namespace App\Pokemon;

use App\Http\SimpleHTTPClient;
use App\Log\EchoLogger;

class PokemonApi
{
    private const BASE_API_URL = "https://pokeapi.co/api/v2";

    /**
     * @var \App\Http\SimpleHTTPClient
     */
    private $_httpClient;

    public function __construct()
    {
        $this->_httpClient = new SimpleHTTPClient();
    }

    public function getPokemon($offset = 0, $limit = 0)
    {
        EchoLogger::log('Getting pokemon with limit ['.$limit.'] and offset ['.$offset.']');

        return $this->_httpClient->sendRequest(
            'GET',
            self::BASE_API_URL."/pokemon/?limit=$limit&offset=$offset",
            [],
            [
                'Content-Type' => 'application/json'
            ]
        );
    }

    public function getPokemonById($pokemonId)
    {
        EchoLogger::log('Getting Pokemon with ID ['.$pokemonId.']');

        return $this->_httpClient->sendRequest(
            'GET',
            self::BASE_API_URL."/pokemon/$pokemonId",
            [],
            [
                'Content-Type' => 'application/json'
            ]
        );
    }
}
