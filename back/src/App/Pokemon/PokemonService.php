<?php declare(strict_types=1);

namespace App\Pokemon;

use App\DI\Container;

class PokemonService
{
    /**
     * @var \App\Pokemon\PokemonApi
     */
    private $_pokemonApi;

    public function __construct()
    {
        $this->_pokemonApi = Container::get('pokemonApi');
    }

    public function getPokemon($offset = 0, $limit = 0)
    {
        return $this->_pokemonApi->getPokemon($offset, $limit);
    }

    public function getPokemonById($pokemonId)
    {
        return $this->_pokemonApi->getPokemonById($pokemonId);
    }
}
