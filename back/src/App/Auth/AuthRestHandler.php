<?php declare(strict_types=1);

namespace App\Auth;

use App\DI\Container;
use App\Log\EchoLogger;
use App\Rest\BadRequestException;
use App\Rest\NotFoundException;
use App\Rest\RequestInfo;
use App\Rest\SimpleRequest;
use App\Rest\SimpleResponse;

class AuthRestHandler extends \App\Rest\AbstractRestHandler
{
    /**
     * @var \App\Session\SessionService
     */
    private $_sessionService;

    /**
     * @var \App\Users\UsersService
     */
    private $_usersService;

    /**
     * @var \App\Pokemon\PokemonService
     */
    private $_pokemonService;

    /**
     * @var \App\Marshalling\MarshallingService
     */
    private $_marshallingService;

    public function __construct()
    {
        $this->_sessionService = Container::get('sessionService');
        
        $this->_usersService = Container::get('usersService');
        $this->_pokemonService = Container::get('pokemonService');

        $this->_marshallingService = Container::get('marshallingService');
    }

    protected function _handle(SimpleRequest $request): SimpleResponse
    {
        $info = new RequestInfo($request);

        if ($request->getMethod() === 'POST' && $info->route('auth/login')) {
            return $this->_handleAuthPathLoginPost($request);
        }

        if ($request->getMethod() === 'POST' && $info->route('auth/logout')) {
            return $this->_handleAuthPathLogoutPost($request);
        }
        
        if ($request->getMethod() === 'POST' && $info->route('auth/signup')) {
            return $this->_handleAuthPathSignupPost($request);
        }

        throw new NotFoundException('Could not map ['.$request->getPathInfo()->getPath().']');
    }

    private function _handleAuthPathLoginPost(SimpleRequest $request): SimpleResponse
    {
        $json = $request->getBody();

        try {
            $user = $this->_usersService->getUserByEmail($json['email']);
        } catch (NotFoundException $e) {
            throw new NotAuthorizedException('No user with this email exists.');
        }

        $existing = $this->_sessionService->findExistingActiveSession($user->getUserId());

        if ($existing) {
           throw new BadRequestException('Already logged in.');
        }

        if ($user->getPassword() !== md5($json['password'])) {
            throw new NotAuthorizedException('Incorrect email or password.');
        }

        // Login successful
        $session_id = $this->_sessionService->createNewSession($user->getUserId());

        try {
            $user_preferences = $this->_usersService->getUserPreferences($user->getUserId());

            $pokemon_details = $this->_pokemonService->getPokemonById($user_preferences['favorite_pokemon_id']);
            $user->setFavoritePokemon($this->_marshallingService->marshalItem($pokemon_details, 'pokemon-short'));
        } catch (NotFoundException $e) {
            EchoLogger::log($e);
            $user->setFavoritePokemon([]);
        }

        $response = new SimpleResponse(($user->getData()));
        $response->setHeader('App-Session-ID', $session_id);

        return $response;
    }

    private function _handleAuthPathLogoutPost(SimpleRequest $request): SimpleResponse
    {
        $to_close = $request->getHeader('App-Session-ID');

        if (empty($to_close)) {
            throw new BadRequestException('No session to close');
        }

        $this->_sessionService->closeSession($to_close);

        return new SimpleResponse([]);
    }

    private function _handleAuthPathSignupPost(SimpleRequest $request): SimpleResponse
    {
        $json = $request->getBody();

        $user_id = $this->_usersService->createNewUser($json['email'], $json['password']);
        $preferences_id = $this->_usersService->createUserPreferences($user_id);

        $user = $this->_usersService->getUserById($user_id);

        $session_id = $this->_sessionService->createNewSession($user_id);
        
        $response = new SimpleResponse($user->getData());
        $response->setHeader('App-Session-ID', $session_id);

        return $response;
    }
}