<?php declare(strict_types=1);

namespace App\Auth;

use Exception;

class NotAuthorizedException extends Exception
{
}