<?php declare(strict_types=1);

namespace App\Rest;

use App\DI\Container;

class RestApp
{
    public function __construct() {}

    /**
     * @TODO: could be static
     * Takes a rest request, handles it, and returns a rest response
     * @param SimpleRequest $request Request to handle
     * @return SimpleResponse The resulting response
     */
    public function handleRequest(SimpleRequest $request)
    {
        $path_info = $request->getPathInfo();
        
        if ($path_info->startsWith('users') || $path_info->startsWith('users-preferences')) {
            $handler = Container::get('usersRestHandler');
        }

        else if ($path_info->startsWith('auth')) {
            $handler = Container::get('authRestHandler');
        }

        else if ($path_info->startsWith('pokemon')) {
            $handler = Container::get('pokemonRestHandler');
        }

        // Dummy
        else if ($path_info->startsWith('hello')) {
            $handler = new class () extends AbstractRestHandler {
                protected function _handle(SimpleRequest $request): SimpleResponse {
                    return new SimpleResponse([
                        'path' => $request->getPathInfo()->getPath().'['.print_r($request->getQueryParams(), true).']'
                    ]);
                }
            };
        }

        else {
            throw new NotFoundException('Could not determine handler for request [' . $request . ']');
        }
        
        /** @var \App\Rest\IRestHandler $handler */
        return $handler->handle($request);
    }
}