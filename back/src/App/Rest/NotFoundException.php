<?php declare(strict_types=1);

namespace App\Rest;

class NotFoundException extends \Exception
{
}