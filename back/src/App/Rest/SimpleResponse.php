<?php declare(strict_types=1);

namespace App\Rest;

class SimpleResponse
{
    private $_data;

    private $_headers;

    public function __construct($data, $headers = []) {
        $this->_data = $data;
        $this->_headers = $headers;
    }

    public function getData($asString = false) {
        return $asString ? json_encode($this->_data) : $this->_data;
    }

    public function getHeaders()
    {
        return $this->_headers;
    }

    public function getHeader($requestedHeader)
    {
        $requestedHeader = strtolower($requestedHeader);

        foreach ($this->_headers as $header => $value) {
            if (strtolower($header) === $requestedHeader) {
                return $value;
            }
        }

        return null;
    }

    public function setHeader($header, $value)
    {
        $this->_headers[$header] = $value;
    }
}