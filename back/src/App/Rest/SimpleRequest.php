<?php declare(strict_types=1);

namespace App\Rest;

use App\Log\EchoLogger;

class SimpleRequest
{
    /**
     * Method of the request
     * @var mixed
     */
    private $_method;

    /**
     * Path without the query parameters
     * @var PathInfo
     */
    private $_path;

    /**
     * Map of query parameters
     * @var array
     */
    private $_queryParams;

    /**
     * Map of body parameters
     * @var array
     */
    private $_body;

    /**
     * Map of headers
     * @var array
     */
    private $_headers;

    public function __construct($method, $path, $queryParams, $body, $headers)
    {
        $this->_method = $method;
        $this->_path = new PathInfo($this->_cleanPath($path));
        $this->_queryParams = $this->_queryStringToArray($queryParams);
        $this->_body = $body;
        $this->_headers = $headers;
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function getPathInfo()
    {
        return $this->_path;
    }

    public function getQueryParams()
    {
        return $this->_queryParams;
    }

    public function getBody()
    {
        return $this->_body;
    }

    public function getHeaders()
    {
        return $this->_headers;
    }

    public function getHeader($header)
    {
        foreach ($this->_headers as $name => $value) {
            if (strtolower($header) === strtolower($name)) {
                return $value;
            }
        }
        
        return null;
    }

    public static function fromServerConstants()
    {
        $body = [];
        if (!empty($_POST)) {
            $body = array_merge($body, $_POST);
        }

        try {
            $input = self::_tryParseInput();
            $body = array_merge($body, $input);
        } catch (\Exception $e) {
        }
        
        return new SimpleRequest(
            $_SERVER['REQUEST_METHOD'],
            $_SERVER['REQUEST_URI'],
            $_SERVER['QUERY_STRING'] ?? '',
            $body,
            self::_getRequestHeaders()
        );
    }

    private function _cleanPath($path)
    {
        if (strpos($path, '/') === 0) {
            $path = substr($path, 1);
        }

        if (strpos($path, '?') !== false) {
            $path = explode('?', $path)[0];
        }

        return $path;
    }

    private function _queryStringToArray($query)
    {
        if (!is_string($query)) {
            throw new \Exception('Expected string to turn into array, got ['.gettype($query).'] instead');
        }

        if (empty($query)) {
            return [];
        }

        if (strpos($query, '?') === 0) {
            $query = substr($query, 1);
        }

        $data = [];
        $params = explode('&', $query);

        foreach ($params as $param) {
            $keyval = explode('=', $param);

            $data[trim($keyval[0])] = trim($keyval[1]);
        }

        return $data;
    }

    private static function _tryParseInput()
    {
        $input = file_get_contents('php://input');

        if ($input === false) {
            throw new \Exception('Failed reading php://input');
        }

        $input = json_decode($input, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Malformed JSON in input: '.json_last_error_msg());
        }

        return $input;
    }

    private static function _getRequestHeaders()
    {
        //https://stackoverflow.com/a/541463/5552688
        $headers = [];
        
        foreach($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }

            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }

        return $headers;
    }

    public function __toString()
    {
        return get_class($this).'['.$this->getMethod().']['.$this->getPathInfo()->getPath().']';
    }
}