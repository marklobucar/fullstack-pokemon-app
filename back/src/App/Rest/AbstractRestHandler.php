<?php declare(strict_types=1);

namespace App\Rest;

abstract class AbstractRestHandler implements IRestHandler
{
    public function handle(SimpleRequest $request): SimpleResponse
    {
        if ($request->getMethod() === 'OPTIONS') {
            // Return empty response just to dump CORS headers
            return new SimpleResponse([]);
        }

        // "Middleware"
        $response = $this->_handle($request);

        $response->setHeader('Content-Type', 'application/json');
        $response->setHeader('Content-Length', strlen($response->getData(true)));

        return $response;
    }

    abstract protected function _handle(SimpleRequest $request): SimpleResponse;
}