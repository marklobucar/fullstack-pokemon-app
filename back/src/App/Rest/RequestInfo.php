<?php declare(strict_types=1);

namespace App\Rest;

class RequestInfo
{
    /**
     * @var SimpleRequest
     */
    private $_request;

    private $_data;

    public function __construct(SimpleRequest $request)
    {
        $this->_request = $request;
    }

    public function route($route)
    {
        $path = $this->_request->getPathInfo()->getPath();

        $path_parts = explode('/', $path);
        $route_parts = explode('/', $route);

        if (count($path_parts) !== count ($route_parts)) {
            // Requested route and actual path do not match
            return false;
        }

        foreach ($route_parts as $index => $part)
        {
            if (strpos($part, '{') === 0)
            {
                // route placeholder
                $matches = [];
                preg_match('/{([\w\-_]*?)}/', $part, $matches);
    
                if (isset($matches[1])) {
                    $this->_data[$matches[1]] = $path_parts[$index];
                }
            }
            else if ($path_parts[$index] !== $part)
            {
                // not placeholder, if parts do not match, route is not match either
                return false;
            }
        }

        return true;
    }

    public function get($key) {
        if (!isset($this->_data[$key])) {
            return null;
        }

        return $this->_data[$key];
    }

    public function __toString()
    {
        return get_class($this).'['.$this->_request.']';
    }
}