<?php declare(strict_types=1);

namespace App\Rest;

class PathInfo
{
    private $_path;

    public function __construct($path)
    {
        $this->_path = $path;
    }

    public function startsWith($what)
    {
        $parts = explode('/', $this->_path);

        return $parts[0] === $what;
    }

    public function getPath()
    {
        return $this->_path;
    }
}