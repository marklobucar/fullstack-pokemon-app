<?php declare(strict_types=1);

namespace App\Marshalling;

interface IMarshaller
{
    public function marshal($data);
}