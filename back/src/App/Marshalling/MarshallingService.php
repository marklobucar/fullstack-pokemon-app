<?php declare(strict_types=1);

namespace App\Marshalling;

use App\Rest\NotFoundException;

class MarshallingService
{
    /**
     * Map of marshallers
     * @var IMarshaller[]
     */
    private $_marshallers;

    public function __construct()
    {
    }

    public function marshalItem($data, $marshallerType)
    {
        if (isset($this->_marshallers[$marshallerType])) {
            return $this->_marshallers[$marshallerType]->marshal($data);
        }

        throw new NotFoundException('No marshaller registered with type ['.$marshallerType.']');
    }

    public function registerMarshaller(IMarshaller $marshaller, $type)
    {
        $this->_marshallers[$type] = $marshaller;
    }
}