<?php declare(strict_types=1);

namespace App\Log;

if (!defined('LOG_DISABLED')) {
	define('LOG_DISABLED', true);
}

class EchoLogger
{
    public static function log($str)
	{
		if (LOG_DISABLED) {
			return;
		}

        if (is_a($str, 'Exception')) {
            $str = self::_formatError($str);
        }

		$backtrace = debug_backtrace();
		$trace = $backtrace[0];

		if (isset($backtrace[2]['class']))
		{
			$info = '['.@$backtrace[2]['class'].':'.@$backtrace[2]['function'].'('.@$backtrace[1]['line'].")]\t";
		}
		else if (!empty($trace))
		{
			$info =	'['.$trace['file'].' '."(" .$trace['line'] .")]\t";
		}
		else
		{
			$info = '';
		}
	
		$str1 = self::_getInfo($info);
		$str1 .= ' '.str_replace("\r\n", "\r\n\t", $str) . "\r\n";
	
		file_put_contents('php://stdout', $str1);
	}
	
	protected static function _formatError(\Exception $error)
	{
		$str = get_class($error).': ' .$error->getMessage()."\r\n";
		$str .= $error->getTraceAsString();

		return $str;
	}
	
	protected static function _getInfo($info)
	{
		$time = microtime();
		$date = ':' . substr($time, 2, 3);
	
		$str = $date . "\t" . $info;
	
		return $str;
	}
}