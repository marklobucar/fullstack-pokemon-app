<?php declare(strict_types=1);

namespace App\DI;

final class Container
{
    private static $_map;

    /**
     * Scan through directories looking for PHP classes and require_once them
     * @param string $root Directory to start scanning in
     */
    public static function init($root = __DIR__)
    {
        $old_include_path = get_include_path();
        set_include_path(realpath($root));

        spl_autoload_register(function ($file) use ($root) {
            require_once(self::_get_real_file_path($root, $file));
        });

        set_include_path($old_include_path.PATH_SEPARATOR.realpath($root));
    }

    public static function get($key)
    {
        if (!isset(self::$_map[$key])) {
            self::$_map[$key] = new Proxy($key);
        }
        
        return self::$_map[$key];
    }

    public static function set($key, $item)
    {
        if (isset(self::$_map[$key]) && self::$_map[$key] instanceof \App\DI\Proxy){
            self::$_map[$key]->set($item);
        } else {
            self::$_map[$key] = $item;
        }
    }

    private static function _get_real_file_path($root, $file)
    {
        $file = $root.'\\'.$file.'.php';
        
        if (file_exists($file)) {
            return $file;
        }

        return false;
    }
}