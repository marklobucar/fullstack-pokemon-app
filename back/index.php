<?php declare(strict_types=1);

use \App\Di\Container;
use App\Log\EchoLogger;
use \App\Rest\RestApp;
use \App\Rest\SimpleRequest;

if (!file_exists(__DIR__.'/__config.php')) {
    die('Missing configuration file.');
}

require_once(__DIR__.'/__config.php');

if (!defined('DUMP_ERROR_DATA')) {
    define('DUMP_ERROR_DATA', false);
}

require_once(__DIR__.'/src/App/DI/Container.php');

Container::init(__DIR__.'/src');
require_once(__DIR__.'/src/di.php'); // DI requiring

ob_start();

$request = SimpleRequest::fromServerConstants();
$response = null;

try
{
    $app = new RestApp();
    $response = $app->handleRequest($request);

    $json = $response->getData(true);
    header('HTTP/1.1 200 OK', true);
}
catch (\App\Rest\BadRequestException $e)
{
    EchoLogger::log($e);

    $json = '{"errorMessage":"'.$e->getMessage().'"}';
    header('Content-Type: application/json', true);
    header('Content-Length: '.strlen($json), true);
    header('HTTP/1.1 400 Bad Request', true);
}
catch (\App\Auth\NotAuthorizedException $e)
{
    EchoLogger::log($e);

    $json = '{"errorMessage":"'.$e->getMessage().'"}';
    header('Content-Type: application/json', true);
    header('Content-Length: '.strlen($json), true);
    header('HTTP/1.1 401 Unauthorized', true);
}
catch (\App\Rest\NotFoundException $e)
{
    EchoLogger::log($e);

    $json = '{"errorMessage":"'.$e->getMessage().'"}';
    header('Content-Type: application/json', true);
    header('Content-Length: '.strlen($json), true);
    header('HTTP/1.1 404 Not Found', true);
}
catch (\Exception $e)
{
    EchoLogger::log($e);

    if (DUMP_ERROR_DATA) {
        $msg = $e->getMessage().PHP_EOL.$e->getTraceAsString();
    } else {
        $msg = "Something went wrong. Please try again later."; 
    }

    $json = '{"errorMessage":"'.$msg.'"}';
    header('Content-Type: application/json', true);
    header('Content-Length: '.strlen($json), true);
    header('HTTP/1.1 500 Internal Server Error', true);
}

header("Access-Control-Allow-Origin: ".BASE_APP_URL, true);
header("Access-Control-Allow-Credentials: true", true);
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS", true);
header("Access-Control-Allow-Headers: Content-Type, Accept, App-Session-ID, X-Pokemon-Count", true);
header("Access-Control-Expose-Headers: App-Session-ID, X-Pokemon-Count", true);

if ($response) {
    foreach ($response->getHeaders() as $header => $value) {
        header($header.": ".$value, true);
    }
}

echo $json;

ob_end_flush();