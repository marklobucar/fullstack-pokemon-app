import { Injectable } from '@angular/core';
import { User } from './user';
import { UserApiService } from "../api/user-api.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private usersApi: UserApiService
  ) { }

  setUser(user: User) {
    localStorage.setItem('pkm-app-user', JSON.stringify(user.getData()));
  }

  removeUser() {
    localStorage.removeItem('pkm-app-user');
  }

  getUser(): User {
    const data = JSON.parse(localStorage.getItem('pkm-app-user') as string);

    return new User().from(data);
  }

  deleteUser() {
    const user = this.getUser();
    return this.usersApi.deleteUser(user.user_id as string);
  }

  setFavoritePokemon(pokemonId) {
    const user = this.getUser();
    return this.usersApi.setFavoritePokemon(user.user_id!, pokemonId);
  }
}
