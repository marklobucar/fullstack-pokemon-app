import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import {UserService} from "../user.service";

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  deleteAccount() {
    if (window.confirm('Are you absolutely sure you want to delete your account?')) {
      this.userService.deleteUser().subscribe(() => {
        this.userService.removeUser();
  
        this.authService.removeSessionId();
  
        this.router.navigate(['login']);
      });
    }
  }

}
