import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  user: User;

  constructor(private router: Router, private userService: UserService) {
    this.user = this.userService.getUser();

    if (!this.user) {
      this.router.navigate(['login'])
    }
  }

  ngOnInit(): void {
  }

}
