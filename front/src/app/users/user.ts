import { Pokemon } from "../pokemon/pokemon";

export class User {
    constructor(public user_id?: string, public email?: string, public favorite_pokemon?: Pokemon) {}

    getData() {
        return {
            user_id: this.user_id,
            email: this.email,
            favorite_pokemon: this.favorite_pokemon
        }
    }

    from(data) {
        this.user_id = data.user_id;
        this.email = data.email;
        
        if (data.favorite_pokemon) {
            this.favorite_pokemon = data.favorite_pokemon;
        }

        return this;
    }
}
