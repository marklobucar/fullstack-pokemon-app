import { Routes } from "@angular/router";
import { AuthGuardService } from "../auth/auth-guard.service";
import { PokemonListComponent } from "../pokemon/pokemon-list/pokemon-list.component";
import { UserDetailsComponent } from "../users/user-details/user-details.component";
import { UserSettingsComponent } from "../users/user-settings/user-settings.component";
import { MainViewComponent } from "./main-view/main-view.component";

export const routes: Routes = [
  {
    path: '',
    component: MainViewComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: 'user',
        children: [
          {
            path: 'profile', component: UserDetailsComponent, canActivate: [AuthGuardService],
          },
          {
            path: 'settings', component: UserSettingsComponent, canActivate: [AuthGuardService],
          }
        ]
      },
      {
        path: 'pokemon',
        children: [
          {
            path: 'list', component: PokemonListComponent, canActivate: [AuthGuardService],
          }
        ]
      },
    ]
  },
];