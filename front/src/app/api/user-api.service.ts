import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { User } from '../users/user';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserApiService extends BaseApiService {

  constructor(protected http: HttpClient, protected router: Router) {
    super(http, router);
  }

  getUserById(userId: string) {
    return this.get(`users/${userId}`).pipe(
      map((res) => {
        return new User().from(res.body);
      })
    );
  }

  deleteUser(userId: string) {
    return this.delete(`users/${userId}`);
  }

  setFavoritePokemon(userId: string, pokemonId: string) {
    return this.put(`users-preferences/${userId}`, { favorite_pokemon_id: pokemonId });
  }
}
