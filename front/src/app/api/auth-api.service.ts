import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthApiService extends BaseApiService {

  constructor(
    protected http: HttpClient,
    protected router: Router
  ) { 
    super(http, router);
  }

  login(email: string, password: string) {
    return this.post('auth/login', { email, password });
  }

  logout(sessionId: string) {    
    return this.post('auth/logout', {});
  }

  signup(email: string, password: string) {
    return this.post('auth/signup', { email, password });
  }
}
