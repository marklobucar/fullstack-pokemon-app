import { Injectable } from '@angular/core';
import { HttpClient, HttpContext, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Observable, catchError, throwError, ObservableInput } from 'rxjs';

type ObserveJsonRequestOptions = {
  headers?: HttpHeaders | {
      [header: string]: string | string[];
  };
  observe: 'response';
  context?: HttpContext;
  params?: HttpParams | {
      [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>;
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {

  api_url: string = environment.base_api_url;

  constructor(protected http: HttpClient, protected router: Router) {
  }

  protected prepareUrl(path) {
    return `${this.api_url}/${path}`;
  }

  protected get<T>(url: string, options?: ObserveJsonRequestOptions): Observable<HttpResponse<T>> {
    options = this.setAuthHeader(options);

    return this.http.get<T>(this.prepareUrl(url), { ...options, observe: 'response', responseType: 'json' });
  }

  protected post<T>(url, postBody: any, options?: ObserveJsonRequestOptions): Observable<HttpResponse<T>> {
    options = this.setAuthHeader(options);

    if (options) {
      return this.http.post<T>(this.prepareUrl(url), postBody, options);
    }
    else {
      return this.http.post<T>(this.prepareUrl(url), postBody, { observe: 'response' });
    }
  }

  protected delete<T>(url, options?: ObserveJsonRequestOptions): Observable<HttpResponse<T>> {

    options = this.setAuthHeader(options);

    return this.http.delete<T>(this.prepareUrl(url), options);
  }

  protected put<T>(url, putData, options?: ObserveJsonRequestOptions): Observable<HttpResponse<T>> {
    options = this.setAuthHeader(options);

    return this.http
      .put<T>(this.prepareUrl(url), putData, options);
  }

  protected handleError = (errorResponse : HttpErrorResponse): ObservableInput<any> =>{
    console.log('Error:', errorResponse);
    return throwError(()=> new Error(errorResponse.error));
  }

  protected setAuthHeader(options): ObserveJsonRequestOptions {
    const session_id = localStorage.getItem('app-session-id');

    if (session_id !== null && session_id !== '') {
      let headers = new HttpHeaders();
      headers = headers.set('App-Session-ID', session_id as string);

      options = Object.assign(options || {}, { headers: headers });
    }

    return options;
  }

}
