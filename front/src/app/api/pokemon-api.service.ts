import { Injectable } from '@angular/core';
import {BaseApiService} from "./base-api.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService extends BaseApiService {

  constructor(http: HttpClient, router: Router) {
    super(http, router);
  }

  getPokemonList(limit: number = 0, offset: number = 0) {
    return this.get<{ total: number, data: any }>(`pokemon?limit=${limit}&offset=${offset}`).pipe(
      map((res) => {
        return {
          total: parseInt(res.headers.get('X-Pokemon-Count') as string, 10),
          data: res.body
        };
      })
    );
  }

  getPokemonById(pokemonId: string) {
    return this.get<any>(`pokemon/${pokemonId}`).pipe(
      map((res) => res.body)
    );
  }
}
