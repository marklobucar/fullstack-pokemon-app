import { HttpErrorResponse } from "@angular/common/http";
import { ErrorHandler, Injectable, NgZone } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class AppErrorHandler implements ErrorHandler {

    constructor(private zone: NgZone, private router: Router) { }

    public handleError(error: any): void {
        console.log(error);

        if (error instanceof HttpErrorResponse && error.status === 401) {
            this.zone.run(() => this.router.navigate(['logout']));
        }
    }

}
