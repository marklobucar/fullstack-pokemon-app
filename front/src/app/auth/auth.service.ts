import { Injectable } from '@angular/core';
import {  Router} from '@angular/router';
import { User } from '../users/user';
import { UserService } from '../users/user.service';
import { AuthApiService } from '../api/auth-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private userService: UserService,
    private authApi: AuthApiService
  ) { }

  login(email: string, password: string) {
    return this.authApi.login(email, password);
  }

  logout() {
    const session_id = this.getSessionId();

    return this.authApi.logout(session_id);
  }

  signup(email: string, password: string) {
    return this.authApi.signup(email, password);
  }

  getSessionId(): string {
    return localStorage.getItem('app-session-id') as string;
  }

  setSessionId(sessionId: string) {
    localStorage.setItem('app-session-id', sessionId);
  }

  removeSessionId() {
    localStorage.removeItem('app-session-id');
  }

  checkSession(): boolean {
    const session_id = this.getSessionId();
    return session_id !== null && session_id !== '';
  }
}