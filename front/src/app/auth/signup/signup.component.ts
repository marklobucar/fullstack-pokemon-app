import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/users/user';
import { UserService } from 'src/app/users/user.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signup_form: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService
  ) { 
    this.signup_form = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      repeat_password: new FormControl('')
    })
  }

  ngOnInit(): void {
  }

  signup() {
    const email = this.email?.value;
    const password = this.password?.value;

    return this.authService.signup(email, password).subscribe((res) => {
      this.userService.setUser(new User().from(res.body));
      
      this.authService.setSessionId(res.headers.get('App-Session-ID') as string)

      this.router.navigate([`/user/profile`]);
    });
  }

  validatePasswords() {
    if (/[A-Z]{1,}/.test(this.password?.value) === false || /[^A-Za-z0-9]{1,}/.test(this.password?.value) === false) {
      this.password?.setErrors({
        invalid_password: 'Password must contain at least one capital letter and one special character.',
        ...this.password?.errors || null
      });
    } else {
      this.password?.setErrors(this.password?.errors ? { ...this.password.errors } : null);
    }
    
    if (this.password?.value === this.repeat_password?.value) {
      this.repeat_password?.setErrors(null);
    } else {
      this.repeat_password?.setErrors({ mismatched: true });
    }
  }

  get email() {
    return this.signup_form.get('email');
  }

  get password() {
    return this.signup_form.get('password');
  }

  get repeat_password() {
    return this.signup_form.get('repeat_password');
  }

}
