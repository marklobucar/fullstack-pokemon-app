import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/users/user';
import { UserService } from 'src/app/users/user.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  logging_in: boolean = false;
  error: string | null = '';

  login_form: FormGroup

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService
  ) {
    this.login_form = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
  }

  ngOnInit(): void {
    if (this.authService.checkSession()) {
      this.router.navigate(['pokemon/list']);
    }
  }

  login() {
    this.logging_in = true;

    this.authService.login(
      this.login_form.get('email')?.value, this.login_form.get('password')?.value
    ).subscribe((res) => {
      this.userService.setUser(new User().from(res.body));
      
      this.authService.setSessionId(res.headers.get('App-Session-ID') as string)

      this.router.navigate([`pokemon/list`]);
    }, (error) => {
      console.error(error);

      this.error = error.status === 401 ? error.error.errorMessage : 'Something went wrong. Please try again later.';

      this.logging_in = false;
    });
  }

  get email() {
    return this.login_form.get('email');
  }

  get password() {
    return this.login_form.get('password');
  }

}
