export class Pokemon {
    constructor(private _id?: string, private _name?: string, private _image?: string, private _types?: string[]) {}

    from(data: { id: string, name: string, image: string, types: string[]}) {
        this._id = data.id;
        this._name = data.name;
        this._image = data.image;
        this._types = data.types;

        return this;
    }

    get id() {
        return this._id;
    }

    get name() {
        return this._name!.charAt(0).toUpperCase() + this._name!.slice(1);
    }

    get image() {
        return this._image;
    }

    get types() {
        return this._types;
    }
}
