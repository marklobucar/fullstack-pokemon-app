import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/users/user.service';
import { Pokemon } from '../pokemon';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {

  @Output() onFavorite: EventEmitter<Pokemon> = new EventEmitter();

  @Input() pokemon!: Pokemon;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

  favoritePokemon() {
    this.onFavorite.emit(this.pokemon);
  }

  isPokemonFavorited() {
    return this.userService.getUser().favorite_pokemon?.id === this.pokemon.id;
  }

}
