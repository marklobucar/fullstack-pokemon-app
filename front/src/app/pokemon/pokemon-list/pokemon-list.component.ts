import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/users/user';
import { UserService } from 'src/app/users/user.service';
import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  private _first_call = true;
  loading: { all: boolean, pokemon: boolean } = {
    all: false,
    pokemon: false
  };

  limit: number = 20;

  total: number = 0;
  pokemon: any = {};

  current_page: number = 1;

  pages_total: number = 0;

  constructor(
    private pokemonService: PokemonService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    if (this._first_call) {
      this.loading.all = true;
    }

    this._getPokemon();
  }

  nextPage() {
    if (this.current_page === this.pages_total) {
      return;
    }

    this.current_page += 1;

    this._getPokemon();
  }

  previousPage() {
    if (this.current_page === 1) {
      return;
    }

    this.current_page -= 1;

    this._getPokemon();
  }

  selectPage(page_no) {
    this.current_page = page_no;

    this._getPokemon();
  }

  getPagesRange() {
    return new Array(this.pages_total).fill(0).map((item, index) => index + 1);
  }

  favoritePokemon(pokemon: Pokemon) {
    this.userService.setFavoritePokemon(pokemon.id).subscribe((res) => {
      this.userService.setUser(new User().from(res));
    });
  }

  pageSizeUpdated() {
    this._getPokemon();
  }

  private _getPokemon() {
    this.loading.pokemon = true;

    this.pokemonService.getPokemonList(this.limit, this._calculateActualOffset()).subscribe((res) => {
      this.total = res.total;
      this.pokemon = res.data;

      this.pages_total = Math.ceil(this.total / this.limit);

      this.loading.pokemon = false;

      if (this._first_call) {
        this.loading.all = false;
        this._first_call = false;
      }
    });
  }

  private _calculateActualOffset(): number {
    return this.current_page === 1 ? 0 : (this.current_page - 1) * this.limit;
  }

}
