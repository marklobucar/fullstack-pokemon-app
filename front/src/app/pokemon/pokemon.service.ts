import { Injectable } from '@angular/core';
import {PokemonApiService} from "../api/pokemon-api.service";

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(
    private pokemonApi: PokemonApiService
  ) { }

  getPokemonList(limit: number = 0, offset: number = 0) {
    return this.pokemonApi.getPokemonList(limit, offset);
  }

  getPokemonId(pokemonId) {
    return this.pokemonApi.getPokemonById(pokemonId as string);
  }
}